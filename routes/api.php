<?php

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    // $page = $request->page ?? 1;
    // $users = User::paginate(2, ['*'], 'page', $page);
    $users = User::jsonPaginate(2);
    return UserResource::collection($users);
});

Route::get('/user/{user}', function (Request $request, User $user) {
    return new UserResource($user);
});
