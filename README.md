## Example Api
Sample Application for api user using laravel frameworks

### Requirement
- Install PHP 7.4, NGINX, REDIS, MYSQL
- Docker & Docker Compose (optional)

### Docker Start (optional)
- Masuk ke dalam directory project, lalu jalankan perintah berikut:
```
$ docker-compose up -d
```

### Get Started
- $ cp .env-example .env
- edit .env file
```
adjust APP_URL, DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD, REDIS_HOST, CACHE_DRIVER & SESSION_DRIVER
```
- $ php artisan key:generate

### Initialize database
```
$ php artisan migrate
```

### Seed fake user
```
$ php artisan db:seed
```

### Endpoints
- user list
```
/api/users
/api/users?page[size]=2&page[number]=1
```

- user detail
```
/api/users/1
/api/users/2
```
